import {ModuleWithProviders, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {getAuthServiceConfigs} from './outer-services/facebook/facebook.service';
import {
  SocialLoginModule,
  AuthServiceConfig,
} from "angular5-social-login";
import {AuthGuard} from './common/services/auth-guard.service';
import {CommonModule} from '@angular/common';
import {UserService} from './common/services/user.service';
import {RouterModule, Routes} from '@angular/router';
const routes: Routes = [
  {path: 'login', loadChildren: './login/login.module#LoginModule'},
  {path: '', loadChildren: './main/main.module#MainModule'},
  {path: '**', redirectTo: 'home'},
];
@NgModule({
  imports: [
    SocialLoginModule,
    CommonModule,
    RouterModule.forRoot(routes),
  ],
  exports: [
    SocialLoginModule,
    RouterModule
  ],
  providers: [

  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class FeatureModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: FeatureModule,
      providers : [
        {provide: AuthServiceConfig, useFactory: getAuthServiceConfigs},
        AuthGuard,
        UserService
      ]
    }
  }
}
