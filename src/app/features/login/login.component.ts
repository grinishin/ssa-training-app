import {Component, OnInit} from '@angular/core';
import {
  AuthService,
  FacebookLoginProvider,
} from 'angular5-social-login';
import {Router} from '@angular/router';
import {User} from '../common/models/user';
import {UserService} from '../common/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private socialAuthService: AuthService,
    private router: Router,
    private userService: UserService,
  ) {
  }

  ngOnInit() {
  }

  signIn() {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then(
      (userData: User) => {
        this.userService.login( {
          id: userData.id,
          email: userData.email,
          name: userData.name,
          image: userData.image
        });
        this.router.navigate(['/']);
      })
  }

}
