import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {LoginComponent} from './login.component';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SocialLoginModule} from 'angular5-social-login';
const routes: Routes = [
  {path: '', component: LoginComponent}
];

@NgModule({
  declarations: [
    LoginComponent,
  ],
  imports: [
    SocialLoginModule,
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
  schemas: [
    NO_ERRORS_SCHEMA
  ],
})
export class LoginModule {

}
