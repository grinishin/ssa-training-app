import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TestApiService} from './test.api.service';

@NgModule({
  imports: [
    CommonModule
  ]
})
export class ApiModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ApiModule,
      providers : [
        TestApiService
      ]
    }
  }
}
