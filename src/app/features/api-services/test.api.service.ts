import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {Test} from '../common/models/test';

const TEST_API_KEYS = {
  all_tests: 'alltests'
};

@Injectable()
export class TestApiService {

  getTestList(): Observable<any> {
    return Observable.of(JSON.parse(localStorage.getItem(TEST_API_KEYS.all_tests)) || [])
  }

  addTest(test: Test) {
    const testList: Test[] = JSON.parse(localStorage.getItem(TEST_API_KEYS.all_tests)) || [];
    switch (testList.length) {
      case 0:
        test.id = 0;
        break;
      case 1:
        test.id = testList[0].id + 1;
        break;
      default:
        test.id = testList.sort((a, b) => {
          return a.id > b.id ? -1 : 1
        })[0].id + 1;
    }
    testList.push({
      id: test.id,
      name: test.name,
      questionsCount: test.questions.length,
      dateAdded: test.dateAdded
    });

    console.log(test);
    localStorage.setItem(TEST_API_KEYS.all_tests, JSON.stringify(testList));

    localStorage.setItem(`${test.id}`, JSON.stringify(test));

    return Observable.of(test)
  }

  updateTest(test: Test) {
    const testList: Test[] = JSON.parse(localStorage.getItem(TEST_API_KEYS.all_tests)) || [];
    const index = testList.findIndex(item => item.id == test.id);
    testList[index] = test;

    localStorage.setItem(TEST_API_KEYS.all_tests, JSON.stringify(testList));

    localStorage.setItem(`${test.id}`, JSON.stringify(test));

    return Observable.of(test)
  }

  deleteTest(id: number){
    const testList: Test[] = JSON.parse(localStorage.getItem(TEST_API_KEYS.all_tests)) || [];
    const index = testList.findIndex(item => item.id == id);
    testList.splice(index, 1);

    localStorage.setItem(TEST_API_KEYS.all_tests, JSON.stringify(testList));

    localStorage.removeItem(`${id}`);

    return Observable.of(id)
  }

  getTest(id: number){
    return Observable.of(JSON.parse(localStorage.getItem(`${id}`)))
  }

}
