import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {AuthService} from 'angular5-social-login';
import {UserService} from './user.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private socialAuthService: AuthService,
    private userService: UserService,
    private router: Router
  ){

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>{
    return this.socialAuthService.authState.map(
      res => {
        const canActivate: boolean = Boolean(res) || this.userService.checkUser;
        if (!canActivate) {
          this.router.navigate(['/login'])
        }
        return canActivate;
      }
    )
  }
}
