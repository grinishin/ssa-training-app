import {Injectable} from '@angular/core';
import {User} from '../models/user';

const LOCAL_STORAGE_KEY = 'user';

@Injectable()
export class UserService {
  private _user: User;

  set user(user: User) {
    this._user = user;
  }

  get user(): User {
    //TODO localstorage to backend
    if (!this._user) {
      this.user = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
    }
    return this._user
  }

  login(user: User) {
    this.user = user;
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(user))
  }

  logout() {
    localStorage.removeItem(LOCAL_STORAGE_KEY);
    this.user = undefined;
  }

  get checkUser(): boolean {
    return Boolean(this.user)
  }
}
