export interface MenuItem {
  title: string;
  url: string;
  status: boolean;
  hasChildList?: boolean;
}
