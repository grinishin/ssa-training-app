export interface Test {
  id?: number;
  name: string;
  dateAdded: string;
  questions?: Question[];
  minCorrectAnswers?: number;
  questionsCount?: number;
}

export interface Question {
  questionText: string;
  questionType: QuestionType;
  answers: Answer[];
}

export interface Answer {
  body: string;
  correctness: boolean;
}

export enum QuestionType {
  SINGLE = <any> '0',
  MULTI = <any> '1'
}
