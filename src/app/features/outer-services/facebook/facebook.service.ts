import {
  AuthServiceConfig,
  FacebookLoginProvider,
} from 'angular5-social-login';
import {Config} from '../app.config';


// Configs
export function getAuthServiceConfigs() {
  const config = new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider(Config.Facebook.ID)
      },
    ]
  );
  return config;
}
