import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {MainComponent} from './main.component';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {ModalService} from '../common/services/modal.service';
import {AuthGuard} from '../common/services/auth-guard.service';
import {ApiModule} from '../api-services/api.module';
import {TestsModule} from './tests/tests.module';
import {PassTestComponent} from './tests/test/pass-test/pass-test.component';
import {TestComponent} from './tests/test/test.component';
import {SaveCreateTestFormGuard} from './tests/create-test/save-create-test-form.guard';
import {TestsComponent} from './tests/tests.component';

const mainRoutes: Routes = [
  {
    path: '', component: MainComponent, canActivate: [AuthGuard], children: [

      {path: 'home', loadChildren: './home/home.module#HomeModule'},
      {path: 'tests', loadChildren: './tests/tests.module#TestsModule'},
      // {path: 'tests', component: TestsComponent},
      // {path: 'tests/:id', component: TestComponent, canDeactivate: [SaveCreateTestFormGuard]},
      // {path: 'tests/:id/pass', component: PassTestComponent},
    ]
  },
];

@NgModule({
  declarations: [
    MainComponent,
  ],
  imports: [
    CommonModule,
    ApiModule.forRoot(),
    RouterModule.forChild(mainRoutes),
  ],
  providers: [
    ModalService,
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ],
})
export class MainModule {
}
