import {Component, OnInit} from '@angular/core';
import {AuthService} from 'angular5-social-login';
import {Router} from '@angular/router';
import {User} from '../common/models/user';
import {UserService} from '../common/services/user.service';
import {MenuItem} from '../common/models/menu';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  user: User;

  menu: MenuItem[] = [
    {
      title: 'Home',
      url: '/home',
      status: true
    },
    {
      title: 'Tests',
      url: '/tests',
      status: false
    }
  ];

  constructor(
    private socialAuthService: AuthService,
    private userService: UserService,
              private router: Router) {
  }

  ngOnInit() {
    this.setUser();
  }

  setUser() {
    this.user = this.userService.user;
  }

  logOut() {
    this.socialAuthService.signOut();
    this.router.navigate(['/login']);
  }

}
