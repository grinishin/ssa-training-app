import {Injectable} from '@angular/core';
import {TestApiService} from '../../api-services/test.api.service';
import {Test} from '../../common/models/test';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/do';

@Injectable()
export class TestDaoService {
  constructor(private testService: TestApiService){
    this.getTestList().subscribe()
  }
  private _tests: Test[] = [];

  set tests(test: Test[]){
    this._tests = test;
  }

  get tests(): Test[]{
    return this._tests
  }

  getTestList(){
    return this.testService.getTestList().do(
      res => {
        this.tests = res;
      }
    )
  }

  getTest(id: number): Observable<Test> {
    return this.testService.getTest(id)
  }

  createTest(test: Test) {
    return this.testService.addTest(test).do(
      res => {
        const newTests = this._tests.slice();
        newTests.push(res);
        this.tests = newTests;
      }
    )
  }

  updateTest(test: Test){
    return this.testService.updateTest(test).do(
      res => {
        const index = this._tests.findIndex(item => item.id == res.id);
        const newTests = this._tests.slice();
        newTests[index] = res;
        this.tests = newTests;
      }
    )
  }

  deleteTest(id: number){
    return this.testService.deleteTest(id).do(
      id => {
        this.tests = this._tests.slice().filter(item => item.id != id);
      }
    )
  }
}
