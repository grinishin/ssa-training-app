import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TestsComponent} from './tests.component';
import {PassTestComponent} from './test/pass-test/pass-test.component';
import {TestComponent} from './test/test.component';
import {ResultComponent} from './test/result/result.component';
import {CreateTestComponent} from './create-test/create-test.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ApiModule} from '../../api-services/api.module';
import {RouterModule, Routes} from '@angular/router';
import {SaveCreateTestFormGuard} from './create-test/save-create-test-form.guard';
import {TestDaoService} from './test-dao-service';
import {ModalService} from '../../common/services/modal.service';

const testRoutes: Routes = [
  {path: '', component: TestsComponent},
  {path: ':id', component: TestComponent, canDeactivate: [SaveCreateTestFormGuard]},
  {path: ':id/pass', component: PassTestComponent},
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(testRoutes),
  ],
  declarations: [
    TestsComponent,
    CreateTestComponent,
    TestComponent,
    PassTestComponent,
    ResultComponent,
  ],
  providers: [
    TestDaoService,
    ModalService,
  ],
  entryComponents: [CreateTestComponent]
})
export class TestsModule { }
