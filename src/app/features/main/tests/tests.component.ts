import {Component, ComponentFactoryResolver, OnInit,} from '@angular/core';
import {TestDaoService} from './test-dao-service';
import {CreateTestComponent} from './create-test/create-test.component';
import {ModalService} from '../../common/services/modal.service';
import {Test} from '../../common/models/test';
import {Router} from '@angular/router';


@Component({
  selector: 'app-tests',
  templateUrl: './tests.component.html',
  styleUrls: ['./tests.component.scss']
})
export class TestsComponent implements OnInit {

  constructor(public testDaoService: TestDaoService,
              private componentFactoryResolver: ComponentFactoryResolver,
              private router: Router,
              private modalService: ModalService) {

  }

  ngOnInit() {
    this.getTests();
  }

  getTests() {
    if (this.testDaoService.tests) {
      this.testDaoService.tests = this.testDaoService.tests
    } else {
      this.testDaoService.getTestList().subscribe(
        res => {
          console.log('getTests()');
          this.testDaoService.tests = res;
        }
      )
    }
  }

  openCreator() {
    this.modalService.addDynamicComponent(CreateTestComponent)
  }

  edit(test: Test) {
    this.modalService.addDynamicComponent(CreateTestComponent, test)
  }

  remove(test: Test) {
    if(confirm(`Remove test ${test.name}`)){
      this.testDaoService.deleteTest(test.id).subscribe()
    }
  }

  open(id: number) {
    this.router.navigate(['tests', `${id}`])
  }


}
