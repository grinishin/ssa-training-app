import {Injectable} from '@angular/core';
import {CanDeactivate} from '@angular/router';
import {TestComponent} from '../test/test.component';

@Injectable()
export class SaveCreateTestFormGuard implements CanDeactivate<TestComponent> {

  canDeactivate(component: TestComponent) {
    return component.modelWasChanged ? confirm('Are you sure you want leave without changes') : true;
  }

}
