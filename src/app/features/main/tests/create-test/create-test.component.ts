import {ApplicationRef, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {ModalComponent, ModalData, ModalService} from '../../../common/services/modal.service';
import {TestDaoService} from '../test-dao-service';
import {Test} from '../../../common/models/test';
import {NgForm} from '@angular/forms';

enum Process {
  CREATE = <any> 'Create',
  EDIT = <any> 'Edit'
}

@Component({
  selector: 'app-create-test',
  templateUrl: './create-test.component.html',
  styleUrls: ['./create-test.component.scss']
})
export class CreateTestComponent extends ModalComponent implements OnInit {
  test: Test = {
    name: ``,
    dateAdded: '',
    questions: []
  };
  proccess: Process = Process.CREATE;
  @ViewChild('testName') testNameInput;

  constructor(
    private testDaoService: TestDaoService
  ) {
    super();
  }

  defineTest(){
    if (this.modalData.data) {
      this.proccess = Process.EDIT;
      this.test = this.modalData.data
    } else {
      let id = 1;

      switch (this.testDaoService.tests.length) {
        case 0:
          id = 1;
          break;
        case 1:
          id = this.testDaoService.tests[0].id + 2;
          break;
        default:
          id = this.testDaoService.tests.sort((a, b) => {
            return a.id > b.id ? -1 : 1
          })[0].id + 2;
      }
      this.test.name = `Test ${id}`
    }
  }

  ngOnInit() {
    this.defineTest();
    setTimeout(() => {
      this.testNameInput.nativeElement.select()
    })
  }

  onSubmit(form: NgForm){
    if (form.invalid) return;
    const date = (new Date()).toDateString().split(' ');
    this.test.dateAdded = `${date[2]} ${date[1]} ${date[3]}`;
    this.test.minCorrectAnswers = 1;
    if (this.proccess == Process.EDIT) {
      this.testDaoService.updateTest(this.test).subscribe(
        res => {
          this.refreshTestList();
        }
      )
    } else {
      this.testDaoService.createTest(this.test).subscribe(
        res => {
          this.refreshTestList();
        }
      )
    }
  }

  refreshTestList(){
    this.testDaoService.getTestList().subscribe(
      res => {
        this.destroy()
      }
    );
  }

}
