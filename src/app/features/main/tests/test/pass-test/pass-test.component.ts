import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {QuestionType, Test} from '../../../../common/models/test';
import {TestDaoService} from '../../test-dao-service';

@Component({
  selector: 'app-pass-test',
  templateUrl: './pass-test.component.html',
  styleUrls: ['./pass-test.component.scss']
})
export class PassTestComponent implements OnInit {

  private id: number;
  test: Test;
  activeQuestionNumber: number = 0;
  answerModel: boolean[][] = [];
  questionType = {
    multi: QuestionType.MULTI,
    single: QuestionType.SINGLE
  };

  constructor(private activatedRoute: ActivatedRoute,
              private testDaoService: TestDaoService,
              private router: Router,) {
  }


  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.getTest(this.id);
  }

  getTest(id?) {
    this.testDaoService.getTest(id).subscribe(
      (res: Test) => {
        if (!res) {
          this.router.navigate(['tests']);
          return;
        }
        this.createModel(res)
      }
    );
  }

  createModel(test: Test){
    this.test = test;
    test.questions.forEach(question => {
      this.answerModel.push(question.answers.map((answer) => {
        return false
      }))
    });
  }

  submitAnswer() {
    this.activeQuestionNumber++;
    if (this.activeQuestionNumber < this.test.questions.length - 1) {

    } else {

    }
  }

  radioChanged(selectedIndex: number) {
    const answerModel =  this.answerModel[this.activeQuestionNumber];
    answerModel.forEach((answer, index) => {
      answerModel[index] = index == selectedIndex;
    })
  }

}
