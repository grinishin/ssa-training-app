import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {TestDaoService} from '../test-dao-service';
import {Answer, Question, QuestionType, Test} from '../../../common/models/test';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  testForm: FormGroup;
  private id: number;
  test: Test;
  questionType = {
    multi: QuestionType.MULTI,
    single: QuestionType.SINGLE
  };

  constructor(private formBuilder: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private testDaoService: TestDaoService,) {
  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.getTest(this.id);
  }

  getTest(id?) {
    this.testDaoService.getTest(id).subscribe(
      (res: Test) => {
        this.test = res;
        res ? this.createTestForm(res) : this.router.navigate(['tests']);
      }
    );
  }

  createTestForm(test: Test) {
    this.testForm = this.formBuilder.group({
      "name": [test.name, [Validators.required]],
      "minCorrectAnswers": [test.minCorrectAnswers],
      "questions": this.formBuilder.array([])
    });

    test.questions.forEach((question: Question) => {
      this.createQuestion(question)
    })
  }

  createQuestion(question?: Question){
    const questionToAdd: Question = question ? question : {
      questionText: '',
      questionType: this.questionType.single,
      answers: [{
        body: '',
        correctness: false
      }]
    };
    const formArray = <FormArray>this.testForm.controls["questions"];
    formArray.push(this.formBuilder.group({
      "questionText": [questionToAdd.questionText, [Validators.required]],
      "questionType": [questionToAdd.questionType],
      "answers": this.formBuilder.array([])
    }));

    questionToAdd.answers.forEach((answer: Answer) => {
      let formGroup: any = formArray.controls;
      this.createAnswer(formGroup[formGroup.length - 1], answer)
    })
  }

  createAnswer(questionForm: FormGroup, answer?: Answer){
    const questionToAdd = answer ? answer : {
      body: '',
      correctness: false
    };
    const formArray = questionForm ? <FormArray>questionForm.controls["answers"] : this.formBuilder.array([]);
    formArray.push(this.formBuilder.group({
      "body": [questionToAdd.body, [Validators.required]],
      "correctness": [questionToAdd.correctness]
    }));
  }

  submitTest(testForm: NgForm){
    if (testForm.invalid) return;
    console.dir(testForm.value);
    this.testDaoService.updateTest(Object.assign({
      id: this.id,
      dateAdded: this.test.dateAdded,
      questionsCount: testForm.value.questions.length
    }, testForm.value)).subscribe(
      res => {
        this.test = res;
      }
    )
  }

  checkboxAnswerChangedCorrect(indexOfQuestion: number, indexOfAnswer: number) {
    this.changeRadio(indexOfQuestion, indexOfAnswer, this.questionType.single)
  }

  changeQuestionType(indexOfQuestion: number){
    this.changeRadio(indexOfQuestion, 0, this.questionType.multi)
  }

  changeRadio(indexOfQuestion, indexShouldBeEquallTo, questionTypeShouldBeEquallTo){
    const formArray: any = <FormArray>this.testForm.controls['questions'],
      question: any = formArray.controls[indexOfQuestion],
      answers: any = question.controls['answers'].controls,
      questionType = question.controls['questionType'].value;

    if (questionType == questionTypeShouldBeEquallTo) {

      answers.forEach((answerItem, index) => {
        answerItem.controls.correctness.setValue(index == indexShouldBeEquallTo);
      })
    }
  }

  removeAnswer(questionForm: FormGroup, index: number){
    if (confirm('Are you sure you want to remove this answer?')) {
      const formArray = <FormArray>questionForm.controls["answers"];
      formArray.removeAt(index)
    }
  }

  removeQuestion(index: number){
    if (confirm('Are you sure you want to remove this question?')) {
      const formArray = <FormArray>this.testForm.controls["questions"];
      formArray.removeAt(index)
    }
  }

  get modelWasChanged(): boolean{
    const initialTest: any = Object.assign({}, this.test);
    delete initialTest.dateAdded;
    delete initialTest.id;
    delete initialTest.questionsCount;
    return JSON.stringify(this.testForm.value) != JSON.stringify(initialTest) || !this.testForm;
  }

  openTestToPass(id: number): void {
    this.router.navigate(['tests', this.id, 'pass'])
  }
}
