import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import {AppRoutingModule} from './app.routing.module';
import {FeatureModule} from './features/feature.module';
import {SaveCreateTestFormGuard} from './features/main/tests/create-test/save-create-test-form.guard';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FeatureModule.forRoot()
  ],
  providers: [
    SaveCreateTestFormGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
