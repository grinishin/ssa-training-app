import {RouterModule, Routes} from '@angular/router';
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {AppComponent} from './app.component';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {
    path: '', component: AppComponent
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class AppRoutingModule {
}
